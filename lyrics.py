#!/usr/bin/env python3

import os
import sys
import json
import taglib
import re
import tempfile
import shutil
import subprocess

EDITOR = (
    os.environ.get("EDITOR")
    or shutil.which("micro")
    or shutil.which("nvim")
    or shutil.which("nano")
    or shutil.which("vim")
    or shutil.which("vi")
)

ACT_PRINT = 0
ACT_EDIT = 1
ACT_STDIN = 2

whitespace_or_empty = re.compile(r"^\s*$")

def empty_or_whitespace(string):
    return bool(whitespace_or_empty.match(string))

def help():
    print(f"Usage: {sys.argv[0]} [args] file")
    print("-e       edit")
    print("-i       set lyrics to text from stdin")
    sys.exit()

def parse_args():
    action = ACT_PRINT
    target = ""

    for x in sys.argv[1:]:
        if x in ("-e", "--edit"):
            action = ACT_EDIT
        elif x in ("-i", "--stdin"):
            action = ACT_STDIN
        elif x in ("-h", "--help"):
           help()
        else:
            target = x

    return action, target


def get_text_via_editor(default_text=""):
    tmp = tempfile.NamedTemporaryFile("w+t")
    tmp.write(default_text)
    tmp.flush()

    subprocess.call((EDITOR, tmp.name))

    tmp.seek(0)

    return re.sub(
        r"[\t ]+$",
        "",
        tmp.read(),
        flags=re.MULTILINE
    )

def pop_all(m, *keys):
    s = []
    for x in keys:
        s += m.pop(x, [])
    return s

def main():
    if EDITOR == None:
        sys.exit(
            "Couldn't find a suitable editor. EDITOR environment variable is empty."
        )

    action, target = parse_args()

    if not os.path.isfile(target):
        help()

    t = taglib.File(target)

    lyrics = list(
        filter(
            lambda x: not empty_or_whitespace(x),
            pop_all(t.tags, "LYRICS", "LYRICS:NONE", "UNSYNCEDLYRICS"),
        )
    )

    if len(lyrics) == 0:
        lyrics_text = ""
    else:
        lyrics_text = "\n\n\n".join(lyrics)

    if action == ACT_PRINT:
        print(lyrics_text.rstrip())
        return

    elif action == ACT_EDIT:
        new_lyrics_text = get_text_via_editor(lyrics_text)
        if lyrics_text == new_lyrics_text:
            return
        lyrics_text = new_lyrics_text

    elif action == ACT_STDIN:
        max_data = 2 ** 20  # 1MB
        lyrics_text = sys.stdin.read(max_data)
        if len(lyrics_text) >= max_data:
            sys.exit("Too much data in stdin")

    # LYRICS:NONE ans such are removed via tags.pop() above
    t.tags["LYRICS"] = [lyrics_text]

    t.save()


if __name__ == "__main__":
    main()
